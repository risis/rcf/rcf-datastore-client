'use strict';

const { DataverseClient } = require('js-dataverse');
const FormData = require('form-data');
const fetch = require('node-fetch');
const fs = require('fs');

module.exports = class RcfDatastoreClient {
  constructor (datastoreApiUrl, datastoreApiToken, unblockKey) {
    this.datastoreApiUrl = datastoreApiUrl;
    this.datastoreApiToken = datastoreApiToken;
    if (unblockKey) {
      this.unblockKey = unblockKey;
    }
    this.client = new DataverseClient(datastoreApiUrl, datastoreApiToken);
  }

  searchDatasets (searchOptions) {
    return this.client.search(searchOptions);
  }

  createDataset (dataverseId, datasetInformation) {
    return this.client.addBasicDataset(dataverseId, datasetInformation);
  }

  deleteDataset (datasetId) {
    return this.client.deleteDataset(datasetId);
  }

  publishDataset (datasetId) {
    return this.client.publishDataset(datasetId);
  }

  updateDataset (datasetId, datasetInformation) {
    return this.client.updateDataset(datasetId, datasetInformation);
  }

  uploadFile (datasetId, filepath, filename) {
    const formData = new FormData();
    formData.append('file', fs.createReadStream(filepath), filename);
    formData.append('jsonData', JSON.stringify({ restrict: 'false' }));
    const options = {
      method: 'POST',
      body: formData,
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/:persistentId/add?persistentId=${datasetId}`, options);
  }

  deleteFile (fileId) {
    const options = {
      method: 'DELETE',
      headers: {
        Authorization: 'Basic ' + Buffer.from(this.datastoreApiToken + ':').toString('base64')
      }
    };
    return fetch(`${this.datastoreApiUrl}/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/${fileId}`, options);
  }

  replaceFile (fileId, filepath, filename) {
    const fileBuffer = fs.createReadStream(filepath);
    const jsonData = {
      restrict: 'false',
      forceReplace: 'true'
    };
    return this.client.replaceFile(fileId, filename, fileBuffer, jsonData);
  }

  downloadFile (fileId) {
    return this.client.getFile(fileId);
  }

  downloadDatasetFiles (datasetId) {
    return this.client.getDatasetFiles(datasetId);
  }

  editDataset (datasetId, field, value) {
    const payload = {
      fields: [{
        typeName: field,
        value: value.value
      }]
    };
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'X-Dataverse-key': this.datastoreApiToken
      },
      body: JSON.stringify(payload)
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/:persistentId/editMetadata?persistentId=${datasetId}&replace=true`, options);
  }

  getDatasetInformation (datasetId) {
    const doi = datasetId.replace(/^doi:/, '');
    return this.client.getLatestDatasetInformationFromDOI(doi);
  }

  async listDatasets (dataverseId) {
    if (Array.isArray(dataverseId)) {
      const datasets = await Promise.all(dataverseId.map(id => this.client.listDatasets(id)));
      return datasets.map(dataset => {
        if (dataset.data && dataset.data.data) {
          return dataset.data.data;
        } else {
          throw new Error('data not present in response');
        }
      }).flat();
    } else {
      const dataset = await this.client.listDatasets(dataverseId);
      if (dataset.data && dataset.data.data) {
        return dataset.data.data;
      } else {
        throw new Error('data not present in response');
      }
    }
  }

  getDatasetsInformation (ids) {
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return Promise.all(ids.map(id => fetch(`${this.datastoreApiUrl}/api/datasets/${id}`, options)));
  }

  downloadFiles (ids) {
    const options = {
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/access/datafiles/${ids.join(',')}`, options);
  }

  createDataverse (dataverseParent, dataverseInformation) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(dataverseInformation)
    };
    return fetch(`${this.datastoreApiUrl}/api/dataverses/${dataverseParent}?key=${this.datastoreApiToken}`, options);
  }

  deleteDataverse (dataverseId) {
    const options = {
      method: 'DELETE',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/dataverses/${dataverseId}`, options);
  }

  publishDataverse (dataverseId) {
    const options = {
      method: 'POST',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/dataverses/${dataverseId}/actions/:publish`, options);
  }

  /**
  user information:
  const userInformation = {
    firstName: "Lisa",
    lastName: "Simpson",
    userName: "lsimpson",
    affiliation: "Springfield",
    position: "Student",
    email: "lsimpson@mailinator.com"
  }

  HTTP 200 response:
  {
    status: 'OK',
    data: {
      user: { id: 14, userName: 'Aileen5' },
      authenticatedUser: {
        id: 14,
        identifier: '@Aileen5',
        displayName: 'Hoyt Gleichner',
        firstName: 'Hoyt',
        lastName: 'Gleichner',
        email: 'Johnpaul_Sanford@gmail.com',
        superuser: false,
        affiliation: 'Okuneva - Bailey',
        position: 'Legacy Identity Coordinator',
        persistentUserId: 'Aileen5',
        createdTime: '2021-05-03T18:16:15Z',
        lastLoginTime: '2021-05-03T18:16:15Z',
        authenticationProviderId: 'builtin'
      },
      apiToken: '1c52d4f3-976d-4559-a757-00bc7bbf7b46'
    }
  }
  */
  getUser (userId) {
    const options = {
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken,
        'Content-Type': 'application/json'
      }
    };
    let url = `${this.datastoreApiUrl}/api/admin/authenticatedUsers/${userId}`;
    if (this.unblockKey) {
      url = `${url}?unblock-key=${this.unblockKey}`;
    }
    return fetch(url, options);
  }

  createBuiltinUser (userInformation, userPassword, builtinUsersKey) {
    const options = {
      method: 'POST',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userInformation)
    };
    return fetch(`${this.datastoreApiUrl}/api/builtin-users?password=${userPassword}&key=${builtinUsersKey}`, options);
  }

  createAuthenticatedUser (userInformation) {
    const options = {
      method: 'POST',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userInformation)
    };
    let url = `${this.datastoreApiUrl}/api/admin/authenticatedUsers`;
    if (this.unblockKey) {
      url = `${url}?unblock-key=${this.unblockKey}`;
    }
    return fetch(url, options);
  }

  locks (id) {
    const options = {
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/${id}/locks`, options);
  }

  locksFromDOI (datasetId) {
    const options = {
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/:persistentId/locks?persistentId=${datasetId}`, options);
  }

  listDatasetRoleAssignments (datasetId) {
    const options = {
      method: 'GET'
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/:persistentId/assignments?persistentId=${datasetId}&key=${this.datastoreApiToken}`, options);
  }

  assignDatasetRole (datasetId, roleAssignee) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(roleAssignee)
    };
    return fetch(`${this.datastoreApiUrl}/api/datasets/:persistentId/assignments?persistentId=${datasetId}&key=${this.datastoreApiToken}`, options);
  }

  // see https://guides.dataverse.org/en/latest/api/native-api.html#list-users
  listUsers (searchTerm) {
    const options = {
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    let url = `${this.datastoreApiUrl}/api/admin/list-users?searchTerm=${searchTerm}`;
    if (this.unblockKey) {
      url = `${url}&unblock-key=${this.unblockKey}`;
    }
    return fetch(url, options);
  }

  async assignUserRole (userEmail, roleAlias, datasetId) {
    const resUser = await this.listUsers(userEmail);
    const jsonUser = await resUser.json();
    if (jsonUser.data.users.length > 0) {
      const role = { assignee: `@${jsonUser.data.users[0].userIdentifier}`, role: roleAlias };
      return this.assignDatasetRole(datasetId, role);
    } else {
      throw new Error(`no user found with email '${userEmail}'`);
    }
  }

  async getFileMetadata (datasetId, fileId) {
    const datasetInformation = await this.getDatasetInformation(datasetId);
    let draftVersion = true;
    if (datasetInformation.data.data.latestVersion.versionState === 'RELEASED') {
      draftVersion = false;
    }
    return this.client.getFileMetadata(fileId, draftVersion);
  }

  streamDownloadFile (fileId, options) {
    const fetchOptions = {
      ...options,
      method: 'GET',
      headers: {
        'X-Dataverse-key': this.datastoreApiToken
      }
    };
    let url = `${this.datastoreApiUrl}/api/access/datafile/${fileId}`;
    return fetch(url, fetchOptions);
  }
};
