'use strict';

require('dotenv').config();
const assert = require('assert');
const faker = require('faker');
const util = require('util');
const setTimeoutPromise = util.promisify(setTimeout);

describe('RcfDatastoreClient', () => {
  const RcfDatastoreClient = require('../');
  const { DataverseClient } = require('js-dataverse');

  // ******* HELPER FUNCTIONS *******

  async function createAuthenticatedUser (identifier) {
    const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
    const userInformation = {
      authenticationProviderId: 'orcid',
      persistentUserId: faker.random.uuid(),
      identifier: `@${identifier}`,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email()
    };
    const res = await client.createAuthenticatedUser(userInformation);
    return res;
  }

  function generateDatasetInformation () {
    const datasetInformation = {
      title: faker.lorem.sentence(),
      descriptions: [
        {
          text: faker.lorem.sentences(),
          date: faker.date.recent()
        }
      ],
      authors: [
        {
          fullname: faker.name.findName()
        }
      ],
      contact: [
        {
          email: faker.internet.email(),
          fullname: faker.name.findName()
        }
      ],
      subject: ['Other']
    };
    return datasetInformation;
  }

  async function createDataset (dataverse = 'root') {
    const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
    const datasetInformation = generateDatasetInformation();
    const res = await client.createDataset(dataverse, datasetInformation);
    return res;
  }

  async function createBuiltinUser (token = process.env.DATASTORE_TOKEN) {
    const client = new RcfDatastoreClient(process.env.DATASTORE_URL, token);
    const userInformation = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      userName: faker.internet.userName(),
      affiliation: faker.company.companyName(),
      position: faker.name.jobTitle(),
      email: faker.internet.email()
    };
    const res = await client.createBuiltinUser(userInformation, faker.internet.password(), 'burrito');
    return await res.json();
  }

  async function waitFinalizePublication (datasetId) {
    const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
    let locksJson = {};
    let lockType = 'finalizePublication';
    while (lockType === 'finalizePublication') {
      const locksRes = await client.locksFromDOI(datasetId);
      locksJson = await locksRes.json();
      lockType = locksJson && locksJson.data && locksJson.data[0] ? locksJson.data[0].lockType : '';
      await setTimeoutPromise(500);
    }
    return locksJson;
  }

  // ******* TESTS *********

  it('should instance object on calling constructor', () => {
    const client = new RcfDatastoreClient(faker.internet.url(), faker.random.uuid());
    assert(client instanceof RcfDatastoreClient);
  });

  it('should instance js-dataverse client on constructor', () => {
    const client = new RcfDatastoreClient(faker.internet.url(), faker.random.uuid());
    assert(client.client instanceof DataverseClient);
  });

  it('should return promises from js-dataverse client', () => {
    const client = new RcfDatastoreClient(faker.internet.url(), faker.random.uuid());
    const datasetId = faker.random.number();
    const fileId = faker.random.number();
    assert(client.searchDatasets(faker.random.word()) instanceof Promise);
    assert(client.createDataset(datasetId, faker.random.objectElement()) instanceof Promise);
    assert(client.deleteDataset(datasetId) instanceof Promise);
    assert(client.publishDataset(datasetId) instanceof Promise);
    assert(client.updateDataset(datasetId, faker.random.objectElement()) instanceof Promise);
    assert(client.uploadFile(datasetId, faker.system.filePath(), faker.system.fileName()) instanceof Promise);
    assert(client.deleteFile(fileId) instanceof Promise);
    assert(client.uploadFile(fileId, faker.system.filePath(), faker.system.fileName()) instanceof Promise);
    assert(client.downloadFile(fileId) instanceof Promise);
    assert(client.downloadDatasetFiles(datasetId) instanceof Promise);
  });

  describe('user management', () => {
    it('should get authenticated user', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const userIdentifier = faker.internet.userName();
        await createAuthenticatedUser(userIdentifier);
        const res = await client.getUser(userIdentifier);
        const json = await res.json();
        assert.equal(json.status, 'OK');
        assert.equal(json.data.identifier, `@${userIdentifier}`);
      } else {
        this.skip();
      }
    });

    it('should fail with error 400 message not found when user doesnt exist', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const userId = 'non existent user';
        const res = await client.getUser(userId);
        const json = await res.json();
        assert.equal(res.status, 400);
        assert.equal(json.status, 'ERROR');
        assert.match(json.message, /not found/);
      } else {
        this.skip();
      }
    });

    it('should create builtin user', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
        const userInformation = {
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
          userName: faker.internet.userName(),
          affiliation: faker.company.companyName(),
          position: faker.name.jobTitle(),
          email: faker.internet.email()
        };
        const res = await client.createBuiltinUser(userInformation, faker.internet.password(), 'burrito');
        assert.equal(res.status, 200);
      } else {
        this.skip();
      }
    });

    it('should create authenticated user', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const userInformation = {
          authenticationProviderId: 'orcid',
          persistentUserId: faker.random.uuid(),
          identifier: `@${faker.internet.userName()}`,
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
          email: faker.internet.email()
        };
        const res = await client.createAuthenticatedUser(userInformation);
        const json = await res.json();
        assert.equal(json.status, 'OK');
      } else {
        this.skip();
      }
    });

    it('search user by email', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const email = 'dataverse@mailinator.com';
        const res = await client.listUsers(email);
        const json = await res.json();
        assert.equal(json.status, 'OK');
        assert(json.data.users.length > 0, `no user found with searchTerm = '${email}'`);
      } else {
        this.skip();
      }
    });
  });

  describe('listDatasets', () => {
    it('list datasets from many dataverses at once', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);

        const dataverseInformation1 = {
          alias: 'dataverse-test-1',
          name: 'DATAVERSE test 1',
          permissionRoot: false,
          description: 'DATAVERSE test 1',
          dataverseContacts: [
            {
              contactEmail: 'it@risis.io'
            }
          ],
          dataverseSubjects: ['Research Group']
        };
        let res = await client.createDataverse('root', dataverseInformation1);
        let json = await res.json();
        assert.equal(json.status, 'OK');

        const dataverseInformation2 = {
          alias: 'dataverse-test-2',
          name: 'DATAVERSE test 2',
          permissionRoot: false,
          description: 'DATAVERSE test 2',
          dataverseContacts: [
            {
              contactEmail: 'it@risis.io'
            }
          ],
          dataverseSubjects: ['Research Group']
        };
        res = await client.createDataverse('root', dataverseInformation2);
        json = await res.json();
        assert.equal(json.status, 'OK');

        res = await client.listDatasets(['dataverse-test-1', 'dataverse-test-2']);
        assert.equal(res.length, 0);

        const datasetInformation1 = {
          title: 'Demographic data of planet Mars (1)',
          descriptions: [
            {
              text: 'Data about habitants in mars',
              date: '2030-12-24'
            }
          ],
          authors: [
            {
              fullname: 'Martian Lennon Smith Jr.'
            }
          ],
          contact: [
            {
              email: 'lennon.smith@martian.universe.com',
              fullname: 'Martian Lennon Smith'
            }
          ],
          subject: ['Other']
        };
        res = await client.createDataset('dataverse-test-1', datasetInformation1);
        assert.equal(res.data.status, 'OK');
        datasetInformation1.doi = res.data.data.persistentId;

        const datasetInformation2 = {
          title: 'Demographic data of planet Mars (2)',
          descriptions: [
            {
              text: 'Data about habitants in mars',
              date: '2030-12-24'
            }
          ],
          authors: [
            {
              fullname: 'Martian Lennon Smith Jr.'
            }
          ],
          contact: [
            {
              email: 'lennon.smith@martian.universe.com',
              fullname: 'Martian Lennon Smith'
            }
          ],
          subject: ['Other']
        };
        res = await client.createDataset('dataverse-test-2', datasetInformation2);
        assert.equal(res.data.status, 'OK');
        datasetInformation2.doi = res.data.data.persistentId;

        res = await client.listDatasets(['dataverse-test-1', 'dataverse-test-2']);
        assert.equal(res.length, 2);

        await client.deleteDataset(datasetInformation1.doi);
        await client.deleteDataverse('dataverse-test-1');
        await client.deleteDataset(datasetInformation2.doi);
        await client.deleteDataverse('dataverse-test-2');
      } else {
        this.skip();
      }
    });

    it('list datasets thown if response data is not found', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
        assert.rejects(client.listDatasets('dataverse-not-exists'));
      } else {
        this.skip();
      }
    });
  });

  it('upload file', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const datasetInformation = {
        title: faker.lorem.words(),
        descriptions: [
          {
            text: 'Data about habitants in mars',
            date: '2030-12-24'
          }
        ],
        authors: [
          {
            fullname: 'Martian Lennon Smith Jr.'
          }
        ],
        contact: [
          {
            email: 'lennon.smith@martian.universe.com',
            fullname: 'Martian Lennon Smith'
          }
        ],
        subject: ['Other']
      };
      let res = await client.createDataset('root', datasetInformation);
      assert.equal(res.data.status, 'OK');
      // $ md5sum test/samples/cortext-summit-2021.png
      // 3170973b496cb6147e30dd95fe54db25  test/samples/cortext-summit-2021.png
      res = await client.uploadFile(res.data.data.persistentId, './test/samples/cortext-summit-2021.png', 'cortext-summit-2021.png');
      const json = await res.json();
      assert.equal(json.data.files[0].dataFile.checksum.value, '3170973b496cb6147e30dd95fe54db25');
    } else {
      this.skip();
    }
  });

  describe('permission management', () => {
    it('list dataset roles', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
        const res = await createDataset();
        const datasetId = res.data.data.persistentId;
        const resRoles = await client.listDatasetRoleAssignments(datasetId);
        const json = await resRoles.json();
        assert.equal(json.status, 'OK');
        assert(json.data.find(role => role.assignee === '@dataverseAdmin'), `no role @dataverseAdmin found in ${JSON.stringify(json.data)}`);
      } else {
        this.skip();
      }
    });

    it('add role to dataset', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
        const res = await createDataset();
        const datasetId = res.data.data.persistentId;
        const role = { assignee: '@dataverseAdmin', role: 'curator' };

        const resRoles = await client.listDatasetRoleAssignments(datasetId);
        const jsonRoles = await resRoles.json();
        assert(jsonRoles.data.every(r => r.assignee !== role.assignee || r._roleAlias !== role.role),
          `be sure role '${role.role}' is not assignee to '${role.assignee}' on dataset '${datasetId}'`);

        const resAssign = await client.assignDatasetRole(datasetId, role);
        const jsonAssign = await resAssign.json();
        assert.equal(jsonAssign.status, 'OK');
        assert.equal(jsonAssign.data.assignee, role.assignee);
      } else {
        this.skip();
      }
    });

    it('add role to dataset by user email', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const res = await createDataset();
        const datasetId = res.data.data.persistentId;

        const email = 'dataverse@mailinator.com';
        const resUser = await client.listUsers(email);
        const jsonUser = await resUser.json();
        const role = { assignee: `@${jsonUser.data.users[0].userIdentifier}`, role: 'curator' };

        const resAssign = await client.assignDatasetRole(datasetId, role);
        const jsonAssign = await resAssign.json();
        assert.equal(jsonAssign.status, 'OK');
        assert.equal(jsonAssign.data.assignee, role.assignee);
        assert.equal(jsonAssign.data._roleAlias, role.role);
      } else {
        this.skip();
      }
    });

    it('assign role to a user on a dataset by user email', async function () {
      if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN && process.env.DATASTORE_UNBLOCK_KEY) {
        const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN, process.env.DATASTORE_UNBLOCK_KEY);
        const res = await createDataset();
        const datasetId = res.data.data.persistentId;

        const resBefore = await client.listDatasetRoleAssignments(datasetId);
        const jsonBefore = await resBefore.json();
        const rolesBeforeCount = jsonBefore.data.length;

        const email = 'dataverse@mailinator.com';
        const roleAlias = 'curator';
        const resAdd = await client.assignUserRole(email, roleAlias, datasetId);
        const jsonAdd = await resAdd.json();
        assert.equal(jsonAdd.status, 'OK');

        const resAfter = await client.listDatasetRoleAssignments(datasetId);
        const jsonAfter = await resAfter.json();
        const rolesAfterCount = jsonAfter.data.length;

        assert(rolesBeforeCount < rolesAfterCount, `unsatisfied expectation: ${rolesBeforeCount} < ${rolesAfterCount}`);
      } else {
        this.skip();
      }
    });
  });

  it('get dataset information', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;
      const datasetInformationRes = await client.getDatasetInformation(datasetId);
      assert.match(datasetInformationRes.data.data.id.toString(), /\d+/);
    } else {
      this.skip();
    }
  });

  it('dataset on DRAFT version has no version number', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;
      const datasetInformationRes = await client.getDatasetInformation(datasetId);
      assert.ifError(datasetInformationRes.data.data.latestVersion.versionNumber);
      assert.ifError(datasetInformationRes.data.data.latestVersion.versionMinorNumber);
    } else {
      this.skip();
    }
  });

  it('publish a dataset', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;

      const publishRes = await client.publishDataset(datasetId);

      assert.equal(publishRes.data.status, 'OK');
    } else {
      this.skip();
    }
  });

  it('dataset version is 1.0 after first publishing', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;
      await client.publishDataset(datasetId);
      const datasetInformationRes = await client.getDatasetInformation(datasetId);
      assert.equal(datasetInformationRes.data.data.latestVersion.versionNumber, 1);
      assert.equal(datasetInformationRes.data.data.latestVersion.versionMinorNumber, 0);
    } else {
      this.skip();
    }
  });

  it('non-admin user not allowed to get unpublished dataset information if it is not the owner', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;

      const userRes = await createBuiltinUser();
      const userToken = userRes.data.apiToken;
      const userClient = new RcfDatastoreClient(process.env.DATASTORE_URL, userToken);

      let assertFail = null;
      try {
        await userClient.getDatasetInformation(datasetId);
        assertFail = true;
      } catch (e) {
        assertFail = false;
      }
      assertFail ? assert.fail() : assert.ok(true);
    } else {
      this.skip();
    }
  });

  it('allowed to get dataset information as non-admin user if dataset is published', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;

      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      await client.publishDataset(datasetId);
      await waitFinalizePublication(datasetId);

      const userRes = await createBuiltinUser();
      const userToken = userRes.data.apiToken;
      const userClient = new RcfDatastoreClient(process.env.DATASTORE_URL, userToken);

      const datasetInformationRes = await userClient.getDatasetInformation(datasetId);
      assert.match(datasetInformationRes.data.data.id.toString(), /\d+/);
    } else {
      this.skip();
    }
  });

  it('non-admin user can get file metadata from a DRAFT unpublished dataset if it is the owner', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const userRes = await createBuiltinUser();
      const userToken = userRes.data.apiToken;
      const client = new RcfDatastoreClient(process.env.DATASTORE_URL, userToken);

      const datasetInformation = generateDatasetInformation();
      const datasetRes = await client.createDataset('root', datasetInformation);
      const datasetId = datasetRes.data.data.persistentId;

      const fileRes = await client.uploadFile(datasetId, './test/samples/cortext-summit-2021.png', 'cortext-summit-2021.png');
      const fileJson = await fileRes.json();
      const fileId = fileJson.data.files[0].dataFile.id;

      const fileMetadataRes = await client.getFileMetadata(datasetId, fileId);
      assert.equal(fileMetadataRes.data.label, 'cortext-summit-2021.png');
    } else {
      this.skip();
    }
  });

  it('non-admin user can get dataset metadata from any PUBLISHED dataset', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;

      const adminClient = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      await adminClient.publishDataset(datasetId);
      await waitFinalizePublication(datasetId);

      const userRes = await createBuiltinUser();
      const userToken = userRes.data.apiToken;
      const userClient = new RcfDatastoreClient(process.env.DATASTORE_URL, userToken);

      const datasetInformationRes = await userClient.getDatasetInformation(datasetId);
      assert.match(datasetInformationRes.data.data.id.toString(), /\d+/);
    } else {
      this.skip();
    }
  });

  it('non-admin user can get file metadata from any PUBLISHED dataset', async function () {
    if (process.env.DATASTORE_URL && process.env.DATASTORE_TOKEN) {
      const datasetRes = await createDataset();
      const datasetId = datasetRes.data.data.persistentId;

      const adminClient = new RcfDatastoreClient(process.env.DATASTORE_URL, process.env.DATASTORE_TOKEN);
      const fileRes = await adminClient.uploadFile(datasetId, './test/samples/cortext-summit-2021.png', 'cortext-summit-2021.png');
      const fileJson = await fileRes.json();
      const fileId = fileJson.data.files[0].dataFile.id;
      await adminClient.publishDataset(datasetId);
      await waitFinalizePublication(datasetId);

      const userRes = await createBuiltinUser();
      const userToken = userRes.data.apiToken;
      const userClient = new RcfDatastoreClient(process.env.DATASTORE_URL, userToken);
      const fileMetadataRes = await userClient.getFileMetadata(datasetId, fileId);
      assert.equal(fileMetadataRes.data.label, 'cortext-summit-2021.png');
    } else {
      this.skip();
    }
  });
});
