# rcf-datastore-client

Client for Dataverse in RCF, wrapping js-dataverse, and extending with some
new features missing on js-dataverse.

## Running tests

    npm test

## Running tests on top of rcf-local Dataverse instance

**Alert** you need to have `mkcert` installed on your local.

Create the `.env` file with the content below:

```
DATASTORE_URL=https://datastore.risis.localhost
DATASTORE_TOKEN=<PASTE USER TOKEN HERE>
DATASTORE_UNBLOCK_KEY=rcf-local-secret-key
```

Run tests:

    npm test

## Releasing a new version

* Go to main branch, merge dev branch into main
* Make sure the new version is declared in package.json file
  * See the [Semantic Versioning](https://semver.org) specification on how to increment version numbers
* Make sure all automated tests are ok
* Push main branch to Gitlab
* Create a git tag for the new version and push
* Merge main branch back to dev branch and push
* Create a new Release on Gitlab from the new tag
  * https://gitlab.com/risis/rcf/rcf-datastore-client/-/releases

## Publish NPM

Gitlab CI/CD pipeline publish to lib.cortext.net when a new git tag is pushed
but if you want to do it by-hand the steps are:

    ln -s npmrc .npmrc
    export NPM_TOKEN=<PASTE THE NPM AUTH TOKEN HERE>
    npm publish
    rm .npmrc

## NPM package releases available at

* https://lib.cortext.net/-/web/detail/@cortext/rcf-datastore-client

## Example on how to use it

```javascript
const RcfDatastoreClient = require('@cortext/rcf-datastore-client')

const SERVER = "http://datastore.risis.io"
const TOKEN = "PUT THE DATASTORE API TOKEN HERE"

const datastore = new RcfDatastoreClient(SERVER, TOKEN)

let datasetInformation = {
  "title": "Demographic data of planet Mars",
  "descriptions": [
    {
      "text": "Data about habitants in mars",
      "date": "2030-12-24"
    }
  ],
  "authors": [
    {
      "fullname": "Martian Lennon Smith Jr."
    }
  ],
  "contact": [
    {
      "email": "lennon.smith@martian.universe.com",
      "fullname": "Martian Lennon Smith"
    }
  ],
  "subject": [
    "Other"
  ]
}

let dataverseId = "ADD HERE SOME EXISTING DATAVERSE ALIAS"

datastore.createDataset(dataverseId, datasetInformation)
  .then(res => console.log(res.data))
  .catch((err) => console.log(err.message))
```

## License

- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.
